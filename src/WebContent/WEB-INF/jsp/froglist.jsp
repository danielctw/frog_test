<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="cp" value="${pageContext.request.contextPath}" scope="request" />
<html>
<head>
<link rel="stylesheet" type="text/css" href="${cp}/resources/css/bootstrap.css">
<script src='${cp}/resources/js/jquery-3.1.1.js'></script>
<script src='${cp}/resources/js/bootstrap.js'></script>

<title>Pond Management</title>

</head>

<body>
	<div class="container" style="width: 100%;">
		<div class="row">
			<div class="col-sm-12"><jsp:include page="main.jsp" /></div>
		</div>
	</div>
</body>

</html>