<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="cp" value="${pageContext.request.contextPath}" scope="request" />

<html>
<head>
<title>Spring MVC Tutorial Series by Crunchify.com</title>
<style type="text/css">
body {
	background-image: url('http://crunchify.com/bg.png');
}
</style>
</head>
<body>
	<br>


	<div style="text-align: center">
		<h2>Welcome to the world of of Frogs</h2>
		<form id="submitForm" method="post" action="${cp}/World/froglist.html">
			<input type="button" value="Manage Your Frogs" id="GoBtn">
		</form>

	</div>
</body>
<script src='${cp}/resources/js/jquery-3.1.1.js'></script>

<script type="text/javascript">
	$(document).ready(function() {

		$("#GoBtn").click(function() {
			$("#submitForm").submit();
		});

	});
</script>
</html>