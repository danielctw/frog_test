package com.frogsample.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.frogsample.entity.Frog;


@Controller
@RequestMapping("World")
public class FrogController {

	@RequestMapping("/welcome")
	public ModelAndView helloWorld() {
		
		String message = "";

		return new ModelAndView("welcome", "message", message);
	}

	@RequestMapping("/froglist")
	public ModelAndView FrogList() {

		Frog frog = new Frog();

		// Add one dummy frog record for display purpose
		frog.setName("Bernard");
		frog.setGender("M");
		frog.setDob("23/01/2016");
		frog.setDeath("");

		ArrayList<Frog> frogList = new ArrayList<>();
		frogList.add(frog);

		ModelAndView mav = new ModelAndView();

		mav.addObject("frogs", frogList);

		mav.setViewName("froglist");

		return mav;
	}
	
	

	@RequestMapping("/FrogAction")
	public ModelAndView FrogAction(HttpServletRequest request) {
		
		String[] getAddedFrogs = request.getParameterValues("newEntry");
		ArrayList<Frog> list = new ArrayList<>();
		
		for (String a : getAddedFrogs) {
			String frogName = request.getParameter("newfrog"+a);
			String gender = request.getParameter("newGender"+a);
			String dob = request.getParameter("dob"+a);
			String death = request.getParameter("death"+a);
			
			Frog frog = new Frog();
			frog.setName(frogName);
			frog.setGender(gender);
			frog.setDob(dob);
			frog.setDeath(death);
			list.add(frog);
		}
		
		// perform database insert in different class (go to DAO)
		// for example purpose append it to the beginning list (will only work once)
		
		ArrayList<Frog> frogList = new ArrayList<>();
		
		Frog frog = new Frog();

		// Add one dummy frog record for display purpose
		frog.setName("Bernard");
		frog.setGender("M");
		frog.setDob("23/01/2016");
		frog.setDeath("");
		frogList.add(frog);
		frogList.addAll(list);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("frogs", frogList);

		mav.setViewName("froglist");

		return mav;
	}

}