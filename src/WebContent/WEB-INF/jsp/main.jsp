
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="cp" value="${pageContext.request.contextPath}" scope="request" />



<div class="panel-body">
	<button id="AddFrogBtn" class="btn btn-info btn-lg" title="Add">
		<span class="glyphicon glyphicon-plus"></span>
	</button>
	<button class="btn btn-info btn-lg" title="Edit">
		<span class="glyphicon glyphicon-edit"></span>
	</button>
	<!-- <button id="RemoveFrogBtn" class="btn btn-info btn-lg" title="Delete">
		<span class="glyphicon glyphicon-trash"></span>
	</button> -->
	<button id="SaveFrogBtn" class="btn btn-info btn-lg" title="Save">
		<span class="glyphicon glyphicon-floppy-disk"></span>
	</button>

</div>
<div>
	<form id="SaveFrogForm" method="post" action="${cp}/World/FrogAction.html">
	<table id="PowerTable" class="table table-responsive table-bordered table-striped">
		<thead>
			<tr>
				<th><input type="checkbox" /></th>
				<th>Name</th>
				<th>Gender</th>
				<th>Birth</th>
				<th>Death</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${frogs}" var="frog">
				<tr>
					<td><input type="checkbox" /></td>
					<td>${frog.name}</td>
					<td><c:if test="${frog.gender eq 'M'}">Male</c:if> <c:if test="${frog.gender eq 'F'}">Female</c:if></td>
					<td>${frog.dob}</td>
					<td>${frog.death eq '' ? '-' : frog.death}</td>
			</c:forEach>
		</tbody>
	</table>
	</form>
</div>





<script type="text/javascript">
$(document).ready(function() {

	a = 1;

	$("#AddFrogBtn").click(function(event) {
		event.preventDefault();
		var input = "<tr class='newRow'><td><input type='checkbox' checked name='newEntry' value='"+ a + "'>"
				+ "</td><td><div class='input-group'><input type='text' name='newfrog" + a + "' class='form-control' placeholder='Frog Name'></div></td><td><div class='input-group'><select name='newGender" + a + "'><option value='M'>Male</option><option value='F'>Female</option></select></div></td>"
				+ "<td><div class='input-group'><input type='text' name='dob" + a + "'' class='form-control' placeholder='DD/MM/YYYY'></div></td>"
				+ "<td><div class='input-group'><input type='text' name='death" + a + "'' class='form-control' placeholder='DD/MM/YYYY'></div></td>"
				+ "</tr>";
		$('#PowerTable > tbody:last-child')
				.prepend(input);
	});

	$("#RemoveFrogBtn").click(function(event) {
		event.preventDefault();
		
		alert('Note: this function will remove frogs');
	});
	
	$("#SaveFrogBtn").click(function(event) {
		$("#SaveFrogForm").submit();
	});

});
</script>



